#!/usr/bin/env python

"""

This script is written to support both Python 2 and Python 3

"""

import sys

f = open(sys.argv[1], "r")
template = f.read()
f.close()

inpt = sys.stdin.read()

template_old = None
while template_old != template:
	template_old = template
	template = template.replace("<!-- TEMPLATE_TITLE -->", sys.argv[2])
	template = template.replace("<!-- TEMPLATE_BODY -->", "<!-- Template body -->\n" + inpt)
	template = template.replace("<!-- BLOG_TITLE -->", sys.argv[3])
	template = template.replace("<!-- TEMPLATE_URL -->", sys.argv[4])
	template = template.replace("<!-- GIT_SRC -->", sys.argv[5])
print(template)
