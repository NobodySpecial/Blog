
#!/bin/bash

GIT="git"
ext=""
if [[ "$1" == "-t" ]]; then
        ext="-testbed"
	GIT="echo"
fi

source ./config"$ext".sh
cd articles/

# Dependencies:
# - "$GIT" (if you've cloned this repo, you already have this)
# - cURL (installed by default on most *nix systems)
# - Python (2 or 3 should both work. Only tested on 3)
# - Marked (Markdown->HTML converter)
# - htmlmin (HTML minifier)
# - PyRSS2Gen (available via `pip install pyrss2gen`)


echo "### Articles:" > ../articles-html.md
echo >> ../articles-html.md

while read file; do
	echo "- $file"
	marked --gfm --mangle --breaks -i "$file" | python ../template.py ../template.html "$(head -n1 "$file" | cut -d' ' -f2-)" "$BLOG_TITLE" "$BLOG_URL" "$GIT_SRC" | htmlmin > ../html/"$file".html
	echo "# [$(head -n1 "$file" | cut -d' ' -f2-)](/html/$file.html)" >> ../articles-html.md
	while read line; do
		echo "> $line" >> ../preview/html/"$file".tmp
	done < "$file"
	cat ../preview/html/"$file".tmp | head -n3 | tail -n1 | head -c256 > ../preview/html/"$file"
	echo '<span style="color: #666;">...</span>' >> ../preview/html/"$file"
	echo >> ../preview/html/"$file"
	echo "> [Read more](/html/$file.html)" >> ../preview/html/"$file"
	marked --gfm --mangle --breaks -i ../preview/html/"$file" | htmlmin > ../preview/html/"$file".html
	cat ../preview/html/"$file" >> ../articles-html.md
	echo "---" >> ../articles-html.md
	rm ../preview/html/"$file".tmp
done <<< "$(ls -r *.md)"
cd ../pages
while read file; do
	echo "- $file"
	marked --gfm --mangle --breaks -i "$file" | python ../template.py ../template.html "$(head -n1 "$file" | cut -d' ' -f2-)" "$BLOG_TITLE" "$BLOG_URL" "$GIT_SRC" | htmlmin > ../"$file".html
done <<< "$(ls -r *.md)"
cd ..

head -n -1 articles-html.md | marked --gfm --mangle --breaks | python ./template.py ./template.html "$BLOG_TITLE" "$BLOG_TITLE" "$BLOG_URL" "$GIT_SRC" | htmlmin > html/index.html
cp html/index.html index.html

htmlmin style.css style.min.css
python rssgen.py "$BLOG_TITLE" "$BLOG_URL" "$BLOG_TITLE" articles-html.md ./preview

"$GIT" add -A
"$GIT" commit || {
	echo -n "Reset to HEAD? (y/N)"
	read reset
	if [[ "$reset" == "y" ]]; then
		"$GIT" reset --hard HEAD
	fi
	exit
}
"$GIT" push

for file in ./*.html; do
	curl -F "$file=@$file" https://"$(cat ./apikey"$ext".txt)"@neocities.org/api/upload | cat
done

for file in style.min.css; do
	curl -F "$file=@$file" https://"$(cat ./apikey"$ext".txt)"@neocities.org/api/upload | cat
done

for file in */*; do
	curl -F "$file=@$file" https://"$(cat ./apikey"$ext".txt)"@neocities.org/api/upload | cat
done
for file in preview/html/*; do
        curl -F "$file=@$file" https://"$(cat ./apikey"$ext".txt)"@neocities.org/api/upload | cat
done

for file in LICENSE.txt style.css html/index.html index.html README.md rss2.xml; do
	curl -F "$file=@$file" https://"$(cat ./apikey"$ext".txt)"@neocities.org/api/upload | cat
done
