### Articles:

# [My new Minecraft server](/html/0008-my-new-mc-server.md.html)
> I've decided to start running a Minecraft server on one of my systems. The server is primarily meant for LGBTQ+ players (and allies), but is inclusive to everyone (within reason, of course! Neo-nazis are *not* welcome).
<span style="color: #666;">...</span>

> [Read more](/html/0008-my-new-mc-server.md.html)
---
# [LGBTQ+ and Non-binary Experiences (Part 1)](/html/0007-btw-im-nonbinary.md.html)
> I recently came out as non-binary (exactly 1 week ago). This is probably going to be the first in a series of articles discussing my experiences.
<span style="color: #666;">...</span>

> [Read more](/html/0007-btw-im-nonbinary.md.html)
---
# [Git Commit Signing](/html/0006-commit-signing.md.html)
> I recently stumbled upon an article about [git commit signing](https://dlorenc.medium.com/should-you-sign-git-commits-f068b07e1b1f) that I take issue with. The article discusses its security properties from the perspective of the developer, but there's a<span style="color: #666;">...</span>

> [Read more](/html/0006-commit-signing.md.html)
---
# [Impersonation Attempts](/html/0005-my-accounts.md.html)
> Earlier I released a notice on my Git, Matrix, and Mastodon regarding someone who seems to be using my old username to impersonate me. I've been seeing the name SerpentSecurity32 popping up lately, as well as joining a few Matrix rooms I frequent, under <span style="color: #666;">...</span>

> [Read more](/html/0005-my-accounts.md.html)
---
# [Modern Web Design](/html/0004-web-design.md.html)
> I recently stumbled upon a website, [SAFE House](https://www.safehousenm.org) [[archive]](https://web.archive.org/web/20210625150114/https://www.safehousenm.org/) [[screenshot]](/img/fyk.png), which provides a safe place to live for children in abusive f<span style="color: #666;">...</span>

> [Read more](/html/0004-web-design.md.html)
---
# [The 512KB Club](/html/0003-512kb-club.md.html)
> I recently tried to get this site into the [512KB club](https://512kb.club), a set of websites where all the resources on a page are less than 512KB (in case that wasn't obvious by the name). However, when I submitted the PR to get this site added, the r<span style="color: #666;">...</span>

> [Read more](/html/0003-512kb-club.md.html)
---
# [My New OS](/html/0002-My-New-OS.md.html)
> People have been asking me for a while to post an article about Linux hardening on here, and while I think that would be a good article, I have a better idea. Not too long ago, some people who noticed the PARSEC scripts I released a while back said they <span style="color: #666;">...</span>

> [Read more](/html/0002-My-New-OS.md.html)
---
# [My Rules of Cryptography](/html/0001-babbas-rules-of-cryptography.md.html)
> I was recently in a discussion where I was discussing plausible-deniability in full-disk encryption. As always, one person took the opportunity to mention [XKCD 538](https://xkcd.com/538/). The conversation continued. Then, as usually happens, someone el<span style="color: #666;">...</span>

> [Read more](/html/0001-babbas-rules-of-cryptography.md.html)
---
