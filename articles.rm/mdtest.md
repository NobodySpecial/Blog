# Markdown Test

This is to test the Markdown support of the Markdown parser I chose.

- Unordered List
- Item 2

1. Ordered list
2. Item 2
5. Item 5


---

5. Item 5
6. Item 6
7. Item 7

[x] Checkbox 1 (checked)
[ ] Checkbox 2 (unchecked)
[] Checkbox 3 (unchecked)

&check; Checkmark (HTML)
&#x2713; Checkmark (hex character)
&#x2717; Fanxy "x" (hex character)

~~Strikethrough (double tildes)~~

---Strikethrough (triple hyphens)---


| Table | Header 2 | Header 3 |
--------|----------|-----------
|data1|  data 2    | data 3   |
| Data 4| Data 5   | Data 6   |

https://example.com (implicit link)
[explicit link](https://example.com)
![Image](https://imgs.xkcd.com/comics/1991_and_2021.png)
email@implicit.com

<span style="color: green;">Inline HTML</span>

A vulnerable C program (multi-line code block)
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv){
        char thisdoesnothingright[16];
        char shouldusergetaccess[16] = "no";
        char thisalsodoesnothingordoesit[4];
        strcpy(thisdoesnothingright, argv[1]);
        strcpy(thisalsodoesnothingordoesit, argv[2]);
        if(!strcmp(shouldusergetaccess, "yes")){
                printf("Access granted!");
        } else {
                printf("Access denied.");
        }
}
```

A snippet of `inline code`

> A blockquote
> more text on the next line
> > and a nested blockquote
> > on multiple lines
> and exiting the blockquote on the same ling (notice how it doesn't exit)
> > Another nested blockuote
>  
> And going down a level

> A second blockquote
> 
> with a blank line followed by this
> 
> 
> then 2 blank lines followed by this
