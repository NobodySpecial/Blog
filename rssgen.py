#!/usr/bin/env python

import sys, datetime, PyRSS2Gen

feed_title = sys.argv[1]
feed_link = sys.argv[2]
feed_desc = sys.argv[3]

items = sys.argv[4:]
rss_items = []

f = open(sys.argv[4], "r")
for line in f.read().split("\n"):
	try:
		try:
			if(line[0] != "#"): # Only read lines that are a valid list item
				continue
		except Exception: # Blank lines have no 0th element. Those lines are automatically invalid
			continue
		filename = line.split("(")[1].split(")")[0]
		item_link = feed_link + filename
#		g = open(sys.argv[5] + "/" + filename.split("/")[2][:-5], "r")
		print(sys.argv[5] + filename)
		g = open(sys.argv[5] + filename, "r")
		item_desc = g.read()
		g.close()
		rss_items += [
			PyRSS2Gen.RSSItem(
				title = line.split("[")[1].split("]")[0],
				link = item_link,
				description = item_desc,
				guid = PyRSS2Gen.Guid(item_link)
			)
		]
	except Exception:
		pass # Lines that fail to parse must be invalid. Ignore the error
f.close()

rss = PyRSS2Gen.RSS2(
	title = feed_title,
	link = feed_link,
	description = feed_desc,
	items = rss_items
)
rss.write_xml(open("rss2.xml", "w"))
