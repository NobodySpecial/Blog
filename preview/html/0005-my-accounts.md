> Earlier I released a notice on my Git, Matrix, and Mastodon regarding someone who seems to be using my old username to impersonate me. I've been seeing the name SerpentSecurity32 popping up lately, as well as joining a few Matrix rooms I frequent, under <span style="color: #666;">...</span>

> [Read more](/html/0005-my-accounts.md.html)
