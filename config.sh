#!/bin/bash

# This script sets variables specific to a blogger's setup

export BLOG_TITLE="NobodySpecial's Blog"
export BLOG_URL="https://nobodyspecial.neocities.org" # Omit trailing `/`
export GIT_SRC="https://git.envs.net/NobodySpecial/Blog"
