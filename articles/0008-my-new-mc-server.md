# My new Minecraft server

I've decided to start running a Minecraft server on one of my systems. The server is primarily meant for LGBTQ+ players (and allies), but is inclusive to everyone (within reason, of course! Neo-nazis are *not* welcome).

Obviously the server does not welcome everyone...

### Examples of people who have no business on my server include:

- Neo-nazis
  - More broadly, overt racists of any form
- Mysogenists, as well as overt sexists of any form
- Anyone outspokenly anti-LGBTQ+ (obviously, since the server is specifically meant to be a safe place for LGBTQ+ people)
- Anyone who joins the server just to grief/troll, or otherwise engages in mean-spirited behavior
- Anyone who engages in hate speech, makes threats against LGBTQ+ individuals, attempts to incite violence, etc.
- Anyone acting (either individually, or on behalf of an oppressive government) to censor dissenting viewpoints

### Examples of people who I hope to see on the server include:

- Anyone LGBTQ+, as well as allies and people wanting to learn more
- Political activists, anyone with dissenting viewpoints and opinions to express
- Anyone who simply wants to play Minecraft on a server that won't judge them simply for being their selves

Since I, myself, have always very much been a security and privacy person, another somewhat-tangental category of people who I'd like to see on the server is hackers, online privacy activists, and anyone who has heard of "FAANG" or "GAFAM".

No guarantees are made regarding uptime or performance, but it should be reasonably performant and stable. The server's IP to connect is `lgbtq.1337.cx`, and it has a corresponding website at https://lgbtq.1337.cx.


## Note: *My server is currently down. I'm unsure when it will be back online.*

Once my server is back online, this message will be removed
