# My Rules of Cryptography

I was recently in a discussion where I was discussing plausible-deniability in full-disk encryption. As always, one person took the opportunity to mention [XKCD 538](https://xkcd.com/538/). The conversation continued. Then, as usually happens, someone else brought up the comic again. Having just joined the conversation, they were unaware the comic had already been mentioned.

As annoying as it is, it's interesting to note the consistency with which this happens. It's become not only a trend, but a rule. This led me to realize what I refer to as *NobodySpecial's rule of cryptography*:

- In every online conversation about disk encryption, XKCD 538 will be mentioned *more than* once.
	- To be even more specific, one person will mention the XKCD. As the conversation continues, another person who joined the conversation late (and therefore doesn't know the article was already mentioned) will mention the same XKCD again.

And, the second rule, *NobodySpecial's rule of cryptographic redundance*:

- When XKCD 538 is mentioned in an online conversation, the people having the conversation have already seen XKCD 538 multiple times, and don't need to be reminded of it.
	- Seriously, it gets annoying.

The overprevalence of people mentioning this particular XKCD has caused a few effects:

- An otherwise-good webcomic has been tainted by overreferencing.
	- When I first saw that comic (many, many, many years ago. Seriously, that comic is common knowledge. You don't need to keep reminding us of it), I liked it, but through being constantly reminded of it, I now like it significantly less.
- I get a very good idea who to take seriously, since the people posting that comic are also the people who haven't studied cryptography enough to get annoyed by people mentioning it.
	- So thank you for that, you're basically outing yourself as a noob! :D
- The mention of the comic takes away from the conversation that was going on.
	- Seriously. Please stop constantly reminding us of that comic.

If you're reading this article and *haven't* seen that comic before, read it (https://xkcd.com/538). Then, once you've familiarized yourself with the truths it states, please refrain from spamming it in the middle of people's conversations.
