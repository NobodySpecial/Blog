# Modern Web Design

I recently stumbled upon a website, [SAFE House](https://www.safehousenm.org) [[archive]](https://web.archive.org/web/20210625150114/https://www.safehousenm.org/) [[screenshot]](/img/fyk.png), which provides a safe place to live for children in abusive families. While I like what they're doing, one particular design aspect caught my attention, and not in a good way. As I continued thinking about this design aspect, I started to think about other common web design issues, which inspired me to write this article.

<script type="text/javascript">
	var escape = function(){
		document.body.style.display = "none";
	};
</script>
<a href="https://www.google.com" style="color: #fff; background-color: #ff0000; position: fixed; right: 0; top: 0; font-size: 300%;" onclick="escape()">
	ESCAPE
</a>

So, first off, what was the issue I noticed? Was it the fact that they quite obviously used a wix.com theme I've already seen a thousand times? Nope. Was it the fact that the header bar on their desktop site takes up way too much of the screen and isn't even positioned properly? Nope. It's their little red "escape" button, which is intended to allow visitors to conceal their use of the site, but instead only makes things slightly more dangerous for visitors who actually need it.

The point of the aforementioned escape button is to provide visitors a quick way to conceal their activities. This makes sense, considering abusive parents might not like the idea of their kids leaving. Their kid leaving means they can't hurt them, and the inability to hurt their kid takes away their sense of control. As such, the kid trying to leave might be interpreted as an offensive and selfish act. Yeah, it's fucked up, but some people *actually* think that way.

The problem isn't with the idea, it's the execution. The button works reasonably well on desktop, if you overlook the fact that the header bar takes up half the screen on a standard 1366x768 display. However, it takes about a second to load the Google homepage, during which time the original site is still clearly visible. The other issue is on mobile, the button scrolls with the rest of the page. This means a user would have to scroll all the way up, click the unnecessarily-small button, then wait said second for Google to load. For obvious reasons, neither case is exactly great for the intended purpose.

The solution is pretty simple - have a standalone element (independent from the title bar) which is pinned to a fixed position on the screen. When the element is clicked, it runs a JavaScript `onclick` event that hides the page content immediately, while loading the content of its target (in this case, Google). Also, make sure the button is absolutely massive, so as to be easily accessible by even the largest of sausage fingers. The source to do this is pretty simple; just embed the following snippet somewhere in your page:

```
<script type="text/javascript">
	var escape = function(){
		document.body.style.display = "none";
	};
</script>
<a href="https://www.google.com" style="color: #fff; background-color: #ff0000; position: fixed; right: 0; top: 0; font-size: 300%;" onclick="escape()">
	ESCAPE
</a>
```
***Note:** Some versions of Safari on iOS seem to hide the browser's header bar without adjusting the HTML viewport. This leads to the button becoming partially-hidden. While I would consider this a browser bug and not a bug in my code, this nonetheless needs to be accounted for. Possible fixes include:*
1. *Moving the button down slightly. This will give the button a small bit of room to move without going off the screen and becoming hidden*
2. *Moving the button to the bottom of the screen, instead of the top of the screen. This works on the same principal as the first fix*

The reason for the massive size is pretty simple - the larger the target element, the less precise the user must be when moving their mouse to the target element. This means the user can exchange that presision for movement speed, thus allowing them to click the button faster. The button must also invoke JavaScript to hide the page content immediately. This is because the extra time to load the target is extra time where someone can see what's already on the screen. And having it as an independent element at a fixed position means it won't end up interfering with the rest of the site's usability.

But this article is about more than just designing the perfect "escape" button for a website (especially since alt-tab, alt-F4, or win-D can all work just as well). So, after careful consideration (and by that I mean this is off the top of my head), I bring you:

### NobodySpecial's cardinal sins of web design

- Unnecessary JavaScript, escecially when the website is designed in such a way as to depend on it
	- The issues with enabling JavaScript in browsers has already been thoroughly-documented. As such, I won't explain it again here. See [my previous article](/html/0003-512kb-club.md.html), [GNU's The JavaScript Trap](https://www.gnu.org/philosophy/javascript-trap.en.html), [this article on disabling JavaScript](https://www.websitepolicies.com/blog/enable-disable-javascript), and [the NoJS club](https://nojs.club), a club dedicated to websites which don't have any JavaScript at all, which just so happens to include links to more articles explaining why JavaScript is bad
- 3rd-party content, including content hosted by CDNs
	- Any 3rd-party content adds opportunity for users to be tracked, and infrastructure to be hijacked. And while HTML includes security options for detecting tampered-with CDN-hosted content in the user's browser before loading it, this still doesn't address the issue of user tracking. As a general rule, anything 3rd-party should be (a) mirrored as a first-party resource, or (b) not used at all
- JavaScript animations, especially those which can be implemented in pure CSS
	- CSS is an incredibly powerful language. Even more, it's Turing-complete. It's also great for creating cool animations, without requiring users to enable JavaScript. If an animation can be implemented in CSS, there's no need to use JavaScript, and if it can't be implemented in CSS, you're better off implementing something that actually benefits the website, instead of adding cheap frills that no user will even notice.
- 3rd-party captchas, including reCaptcha, and hCaptcha
	- Using 3rd-party captchas turns over control of part of your website to a 3rd-party provider, creating the same issues as 3rd-party content. It's also a great means of tracking users, as well as requiring users to enable JavaScript. This undermines many of the core principals a good website should be designed to uphold.
- CloudFlare, or other similar services
	- CloudFlare inserts itself as a man-in-the-middle (MitM) between the user and the website they visit. This requires users to trust a 3rd-party simply to visit a website. Of course, users rarely get the opportunity to know whether a site is using CloudFlare unless they specifically check for it on every site they visit. This undermines the user's ability to trust the websites they visit.
- Webs, Wix, Weebly, GoDaddy, SquareSpace, etc
	- While these services make it easy for non-technical people to create websites, they also violate every design principal listed on this page, as well as every design principal listed in my previous article. They undermine the very foundation of a secure, decentralized, and open web that respects users. They also make websites look boring - spend enough time dealing with web development and you actually learn to identify what themes websites use at a glance. Thanks to these services, websites are bloated, slow, non-private, insecure, untrustworthy, uncreative, poorly-designed, and boring.

This is not an exhaustive list, but I think it covers the basics. I guess the key takeaway is this: *good web design is a science, not an art*. Any good website accomplishes a goal. Whether it be informing the visitor of how terrible modern websites are, or helping the visitor get away from abusive parents. There *are* right and wrong answers as to how to do things. If you're designing a website, you need to *design* a website. Don't simply *make* one. And remember there's a lot more to web design than your flashy JavaScript animations (that *really* shouldn't require JavaScript).
