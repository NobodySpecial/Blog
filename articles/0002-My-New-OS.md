# My New OS

People have been asking me for a while to post an article about Linux hardening on here, and while I think that would be a good article, I have a better idea. Not too long ago, some people who noticed the PARSEC scripts I released a while back said they were interested in turning PARSEC into an OS. But instead of simply turning PARSEC into an OS, I decided it would be better to improve upon PARSEC significantly.

The new OS is intended to compete with distributions such as Whonix, Qubes, CLIP OS, and Graphene OS in terms of the level of system hardening. Ideally, users would integrate a virtualization-centric workflow when using this OS. Of course, for users who don't want to or can't use VMs primarily, as well as to improve security against VM escapes, the OS also has a hardened low-attack-surface base (unlike Qubes, which uses a relatively unhardened systemd-enabled base).

What features can be expected of the first production-ready build:

- Built on top of a Void Musl base
	- Musl is better than GNU's Libc, and has better support from hardened_malloc
	- ARM support is expected, although that's a long-term goal
- Verified boot (implemented in a way as to not compromise user freedom or require a second system)
	- Notice the difference between verified boot and secure boot. Verified boot provides significantly stronger security guarantees than secure boot
- All the hardening included in PARSEC
	- I also have a couple things in mind to add in addition to PARSEC. You'll see what I mean as time goes on
- Releases will support Wayland, with [X.org](https://x.org) getting second-class support
	- This is a security improvement over distros that give Wayland second-class support to X.org. Although it *does* mean Nvidia compatibility will be limited
- A virtualization-centric workflow
	- Not unlike Qubes, this OS will expect users to operate primarily within VMs
- First-class support for users who don't want to, or can't, virtualize
	- Virtualization is unsuitable for some use cases, and depending on the user's threat model, can expose kernel attack surface via KVM
	- However, virtualization is still highly-recommended, especially for higher threat models. It's up to the user to decide what they need
	- Users may have to sacrifice hardening to get certain apps to work outside of VMs. This is a trade-off the user must choose on a case-by-case basis
	- Users who sacrifice hardening will still get support, but only second-class support
	- Users who don't sacrifice hardening will get first-class support. ***Hooray for security!***

The project's git repo can be found at:
	 https://git.envs.net/NobodySpecial/UnnamedProject
