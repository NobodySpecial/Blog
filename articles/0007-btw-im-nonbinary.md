# LGBTQ+ and Non-binary Experiences (Part 1)

I recently came out as non-binary (exactly 1 week ago). This is probably going to be the first in a series of articles discussing my experiences.

So... Where to begin? I guess chronologically makes sense. Once making the decision to come out as non-binary, the first thing I did was change my Matrix nick to include my pronouns (they/them). Then, I texted everyone in my Signal contacts informing them of the changes. I guess some people might have made a bigger deal of it, and had an entire coming out ceremony, but it was important to me that it not be a huge thing.

If y'all haven't noticed from my previous articles (or, for those of you who've interacted with me on Matrix, from how I act on Matrix), I like to combine an "I don't give a fuck" attitude with an "I *definitely* give a fuck" attitude. I wanted to not only come out as non-binary, but make a statement with how I came out. The idea being to come out in a way that it's "not a big deal" in order to make a statement that it really shouldn't have to be a big deal.

Think about it... People spend their entire lives living under a false identity of who society tells them they should be. They finally decide to be themselves instead of comforming to other people's idiotic expectations. We live in a world where simply refusing to continue being someone you're not is a huge thing to do. Fuck that!

To share my experiences and check an item off my bucket list at the same time, I've decided I'm going to make a "zine" (a homemade mini-magazine) documenting my experiences in a partially-fictional story that I plan to eventually turn into a webcomic. Also, since alligators are my favorite animal and I strongly dislike the human race (You might have guessed I'm trying to avoid simply saying "I hate people"... Oh wait, I just did), the story takes place in a world where humans don't exist and the main characters are all alligators.

### Cover Preview (draft):

![Spider Gatorz cover drawing](/spidergatorz/coverdemo.jpg)

The cover features Perry (with purple and yellow butterfly wings) in the lower left corner.

### Perry the Platygator

![Perry the Platygator original drawing](/spidergatorz/perry.jpg)

Perry is a hybrid between a platypus and an alligator. The original drawing of Perry was created by a friend of mine, and I decided to put Perry in as a character in my zine.

