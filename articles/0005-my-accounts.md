# Impersonation Attempts

Earlier I released a notice on my Git, Matrix, and Mastodon regarding someone who seems to be using my old username to impersonate me. I've been seeing the name SerpentSecurity32 popping up lately, as well as joining a few Matrix rooms I frequent, under a homeserver I used to be known to use (gnuradio.org).

After further analysis, it seems this person isn't attempting to impersonate me. They simply chose a coincidental username that just happens to be the name of my old blog ([Serpent Security](https://serpentsec.1337.cx)).

This wouldn't be the first time someone has tried to impersonate me, and I'm sure it won't be the last. And while I don't believe this is an impersonation attempt, I don't like coincidences, and I like to be on top of things. Which is why I've for a while had a public git repo with a cryptographically-signed list of what accounts belong to me.

https://git.envs.net/NobodySpecial/whoami

I also released a notice on my Mastodon:
https://ioc.exchange/@Serpent27/106627037712992158

My Minisign public key is as follows:
```
untrusted comment: minisign public key 73C675F1EAF79E80
RWSAnvfq8XXGcw5iUd2+q7OWwlITbIKkp5lUPKR3haFhdIWDdXFf1Rla
```

My PGP key fingerprint is as follows:
```2974C1175BF81E46BC4898306D21A9D9F47CC1C8```
