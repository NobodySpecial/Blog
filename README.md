# NobodySpecial's Blog

Welcome to my Gitea repo *I mean blog*. Since I'm no longer publishing to [SerpentSec](https://serpentsec.1337.cx), people asked me to resume blogging. So, here's my blog!
Expect content to be coming soon. Or not. We'll find out soon enough whether I actually bother to post anything here!

A mirror is available at [nobodyspecial.neocities.org](https://nobodyspecial.neocities.org)
A lower-quality, uglier mirror is also available at [learnhowtoresizeawindowdumbass.neocities.org](https://learnhowtoresizeawindowdumbass.neocities.org)
